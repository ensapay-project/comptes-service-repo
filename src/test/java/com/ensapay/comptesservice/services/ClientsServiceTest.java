

package com.ensapay.comptesservice.services;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.within;
    import static org.mockito.ArgumentMatchers.any;
    import static org.mockito.ArgumentMatchers.anyInt;
    import static org.mockito.ArgumentMatchers.anyString;
    import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import org.mockito.stubbing.Answer;

class ClientsServiceTest {

            @Mock
        private com.ensapay.comptesservice.repositories.ClientsRepository mockRepository;
            @Mock
        private com.ensapay.comptesservice.feign.AuthServiceClient mockAuthServiceClient;
            @Mock
        private com.ensapay.comptesservice.services.MailService mockMailService;

    private com.ensapay.comptesservice.services.ClientsService clientsServiceUnderTest;

@BeforeEach
void setUp() throws Exception {
 initMocks(this);
                                clientsServiceUnderTest = new ClientsService(mockRepository,mockAuthServiceClient,mockMailService) ;
}

    @Test
    void testGetClients() throws Exception {
    // Setup
                                                                final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            client.setAccount(account);
        final java.util.List<com.ensapay.comptesservice.entities.Client> expectedResult = java.util.Arrays.asList(client);

            // Configure ClientsRepository.findAll(...).
                                                        final com.ensapay.comptesservice.entities.Client client1 = new com.ensapay.comptesservice.entities.Client();
            client1.setUserId("userId");
            client1.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account1 = new com.ensapay.comptesservice.entities.User();
                                            account1.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account1.setEmail("email");
            account1.setNumeroTel("numeroTel");
            account1.setFirstName("firstName");
            account1.setLastName("lastName");
            client1.setAccount(account1);
        final java.util.List<com.ensapay.comptesservice.entities.Client> clients = java.util.Arrays.asList(client1);
            when( mockRepository .findAll()).thenReturn(clients);

    // Run the test
 final java.util.List<com.ensapay.comptesservice.entities.Client> result =  clientsServiceUnderTest.getClients();

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
    }

    @Test
    void testGetClient() throws Exception {
    // Setup
                                                                final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            client.setAccount(account);
        final java.util.Optional<com.ensapay.comptesservice.entities.Client> expectedResult = java.util.Optional.of(client);

            // Configure ClientsRepository.findById(...).
                                                        final com.ensapay.comptesservice.entities.Client client2 = new com.ensapay.comptesservice.entities.Client();
            client2.setUserId("userId");
            client2.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account1 = new com.ensapay.comptesservice.entities.User();
                                            account1.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account1.setEmail("email");
            account1.setNumeroTel("numeroTel");
            account1.setFirstName("firstName");
            account1.setLastName("lastName");
            client2.setAccount(account1);
        final java.util.Optional<com.ensapay.comptesservice.entities.Client> client1 = java.util.Optional.of(client2);
            when( mockRepository .findById("id")).thenReturn(client1);

    // Run the test
 final java.util.Optional<com.ensapay.comptesservice.entities.Client> result =  clientsServiceUnderTest.getClient("id");

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
    }

    @Test
    void testGetClient_ClientsRepositoryReturnsAbsent() throws Exception {
    // Setup
                                                                final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            client.setAccount(account);
        final java.util.Optional<com.ensapay.comptesservice.entities.Client> expectedResult = java.util.Optional.of(client);
        when( mockRepository .findById("id")).thenReturn(java.util.Optional.empty());

    // Run the test
 final java.util.Optional<com.ensapay.comptesservice.entities.Client> result =  clientsServiceUnderTest.getClient("id");

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
    }

    @Test
    void testCreateClient() throws Exception {
    // Setup
                final com.ensapay.comptesservice.entities.User user = new com.ensapay.comptesservice.entities.User();
            user.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            user.setEmail("email");
            user.setNumeroTel("numeroTel");
            user.setFirstName("firstName");
            user.setLastName("lastName");

         final com.ensapay.comptesservice.entities.Client expectedResult = new com.ensapay.comptesservice.entities.Client();
            expectedResult.setUserId("userId");
            expectedResult.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            expectedResult.setAccount(account);

            // Configure ClientsRepository.save(...).
        final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account1 = new com.ensapay.comptesservice.entities.User();
                                            account1.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account1.setEmail("email");
            account1.setNumeroTel("numeroTel");
            account1.setFirstName("firstName");
            account1.setLastName("lastName");
            client.setAccount(account1);
            when( mockRepository .save(new com.ensapay.comptesservice.entities.Client())).thenReturn(client);

    // Run the test
 final com.ensapay.comptesservice.entities.Client result =  clientsServiceUnderTest.createClient(user);

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
        verify( mockMailService ).sendPasswordRecoveryMail(new com.ensapay.comptesservice.entities.User(),"password");
    }

    @Test
    void testResetPassword() throws Exception {
    // Setup
                final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            client.setAccount(account);

    // Run the test
 clientsServiceUnderTest.resetPassword(client);

        // Verify the results
        verify( mockMailService ).sendPasswordRecoveryMail(new com.ensapay.comptesservice.entities.User(),"password");
    }

    @Test
    void testUpdateClient() throws Exception {
    // Setup
                final com.ensapay.comptesservice.entities.User body = new com.ensapay.comptesservice.entities.User();
            body.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            body.setEmail("email");
            body.setNumeroTel("numeroTel");
            body.setFirstName("firstName");
            body.setLastName("lastName");

         final com.ensapay.comptesservice.entities.Client expectedResult = new com.ensapay.comptesservice.entities.Client();
            expectedResult.setUserId("userId");
            expectedResult.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            expectedResult.setAccount(account);

            // Configure ClientsRepository.findById(...).
                                                        final com.ensapay.comptesservice.entities.Client client1 = new com.ensapay.comptesservice.entities.Client();
            client1.setUserId("userId");
            client1.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account1 = new com.ensapay.comptesservice.entities.User();
                                            account1.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account1.setEmail("email");
            account1.setNumeroTel("numeroTel");
            account1.setFirstName("firstName");
            account1.setLastName("lastName");
            client1.setAccount(account1);
        final java.util.Optional<com.ensapay.comptesservice.entities.Client> client = java.util.Optional.of(client1);
            when( mockRepository .findById("id")).thenReturn(client);

            // Configure ClientsRepository.save(...).
        final com.ensapay.comptesservice.entities.Client client2 = new com.ensapay.comptesservice.entities.Client();
            client2.setUserId("userId");
            client2.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account2 = new com.ensapay.comptesservice.entities.User();
                                            account2.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account2.setEmail("email");
            account2.setNumeroTel("numeroTel");
            account2.setFirstName("firstName");
            account2.setLastName("lastName");
            client2.setAccount(account2);
            when( mockRepository .save(new com.ensapay.comptesservice.entities.Client())).thenReturn(client2);

    // Run the test
 final com.ensapay.comptesservice.entities.Client result =  clientsServiceUnderTest.updateClient("id",body);

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
    }

    @Test
    void testUpdateClient_ClientsRepositoryFindByIdReturnsAbsent() throws Exception {
    // Setup
                final com.ensapay.comptesservice.entities.User body = new com.ensapay.comptesservice.entities.User();
            body.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            body.setEmail("email");
            body.setNumeroTel("numeroTel");
            body.setFirstName("firstName");
            body.setLastName("lastName");

         final com.ensapay.comptesservice.entities.Client expectedResult = new com.ensapay.comptesservice.entities.Client();
            expectedResult.setUserId("userId");
            expectedResult.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account = new com.ensapay.comptesservice.entities.User();
                                            account.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account.setEmail("email");
            account.setNumeroTel("numeroTel");
            account.setFirstName("firstName");
            account.setLastName("lastName");
            expectedResult.setAccount(account);

         when( mockRepository .findById("id")).thenReturn(java.util.Optional.empty());

            // Configure ClientsRepository.save(...).
        final com.ensapay.comptesservice.entities.Client client = new com.ensapay.comptesservice.entities.Client();
            client.setUserId("userId");
            client.setNumeroTel("numeroTel");
                final com.ensapay.comptesservice.entities.User account1 = new com.ensapay.comptesservice.entities.User();
                                            account1.setCreatedTimestamp(new java.util.GregorianCalendar(2019, java.util.Calendar.JANUARY, 1).getTime());
            account1.setEmail("email");
            account1.setNumeroTel("numeroTel");
            account1.setFirstName("firstName");
            account1.setLastName("lastName");
            client.setAccount(account1);
            when( mockRepository .save(new com.ensapay.comptesservice.entities.Client())).thenReturn(client);

    // Run the test
 final com.ensapay.comptesservice.entities.Client result =  clientsServiceUnderTest.updateClient("id",body);

        // Verify the results
 assertThat(result).isEqualTo(expectedResult ) ;
    }

    @Test
    void testDeleteClient() throws Exception {
    // Setup

    // Run the test
 clientsServiceUnderTest.deleteClient("id");

        // Verify the results
        verify( mockRepository ).deleteById("id");
    }
                                                                    }

