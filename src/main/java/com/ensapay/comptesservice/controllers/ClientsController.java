package com.ensapay.comptesservice.controllers;

import com.ensapay.comptesservice.entities.Client;
import com.ensapay.comptesservice.entities.User;
import com.ensapay.comptesservice.repositories.ClientsRepository;
import com.ensapay.comptesservice.services.ClientsService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.authorization.client.util.Http;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/clients")
@AllArgsConstructor
public class ClientsController {

    private ClientsService service;
    private ClientsRepository repository;

    @GetMapping
    public List<Client> getClients() {
        return service.getClients();
    }

    @GetMapping("{id}")
    public Client getClient(@PathVariable String id) {
        return service.getClient(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public Client createClient(@RequestBody  User user) {
        return service.createClient(user);
    }

    @PostMapping("/reset-password")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void resetPassword(@RequestBody  User user) {
        service.resetPassword(
            repository.findByAccount_Email(user.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Email est introuvable"))
        );
    }

    @PutMapping("{id}")
    public Client updateClient(@PathVariable String id, @RequestBody  User user) {
        return service.updateClient(id, user);
    }

    @DeleteMapping("{id}")
    public void deleteClient(@PathVariable String id) {
        service.deleteClient(id);
    }
}
