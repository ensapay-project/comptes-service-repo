package com.ensapay.comptesservice.controllers;

import com.ensapay.comptesservice.entities.Compte;
import com.ensapay.comptesservice.services.ComptesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.List;

@RequestMapping("/comptes")
@RestController
@AllArgsConstructor
public class ComptesController {

    private ComptesService service;

    @GetMapping
    public List<Compte> getComptes() { return service.getComptes(); }

    @GetMapping("{numeroCompte}")
    public Compte getCompte(@PathVariable String numeroCompte) { return service.getCompte(numeroCompte); }

    @PutMapping("{numeroCompte}")
    public Compte updateCompte(@PathVariable String numeroCompte, @RequestBody Compte compte) { return service.updateCompte(numeroCompte, compte); }

    @PostMapping
    public Compte createCompte(@RequestBody Compte compte) { return service.createCompte(compte); }

    @DeleteMapping("{numeroCompte}")
    public void deleteCompte(@PathVariable String numeroCompte) { service.deleteCompte(numeroCompte); }
}
