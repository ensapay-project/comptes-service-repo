package com.ensapay.comptesservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Client {
    @Id
    private String userId;
    private String numeroTel;
    private User account;
//    private String createdBy;

    // Keycloak Principal
}
