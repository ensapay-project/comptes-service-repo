package com.ensapay.comptesservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Compte {
    @Id
    private String numeroCompte;
    private Double soldeFinal;
    private String clientId, intitule;

}
