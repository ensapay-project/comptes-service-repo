package com.ensapay.comptesservice.services;

import com.ensapay.comptesservice.entities.Client;
import com.ensapay.comptesservice.entities.User;
import com.ensapay.comptesservice.feign.AuthServiceClient;
import com.ensapay.comptesservice.repositories.ClientsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang.RandomStringUtils;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
@Log4j2
public class ClientsService {

    ClientsRepository repository;
    AuthServiceClient authServiceClient;
    MailService mailService;

    public List<Client> getClients() {
        return repository.findAll();
    }

    public Optional<Client> getClient(String id) {
        return repository.findById(id);
    }

    public Client createClient(User user) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEnabled(true);
        userRepresentation.setUsername(user.getNumeroTel());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        userRepresentation.setEmail(user.getEmail());

        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UsersResource usersRessource = realmResource.users();
        Response response = usersRessource.create(userRepresentation);
        System.out.printf("Repsonse: %s %s%n", response.getStatus(), response.getStatusInfo());
        System.out.println(response.getLocation());
        RoleRepresentation userRole = realmResource.roles()//
            .get("ROLE_user").toRepresentation();
        String userId = CreatedResponseUtil.getCreatedId(response);
        UserResource userResource = usersRessource.get(userId);
        System.out.printf("User created with userId: %s%n", userId);

        userResource.roles().realmLevel() //
            .add(Arrays.asList(userRole));
        user.setCreatedTimestamp(new Date());

        Client client = new Client();
        client.setNumeroTel(user.getNumeroTel());
        client.setUserId(userId);
        client.setAccount(user);
        resetPassword(client);

        return repository.save(client);
    }

    public void resetPassword(Client client) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UserResource user = realmResource.users().get(client.getUserId());
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        String randomPassword = RandomString.make(8);
        credentialRepresentation.setValue(randomPassword);
        credentialRepresentation.setTemporary(false);
        user.resetPassword(credentialRepresentation);
        log.info("Generated Password : {}", randomPassword);
        mailService.sendPasswordRecoveryMail(client.getAccount(), randomPassword);
    }

    public Client updateClient(String id, User body) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UserResource user = realmResource.users().get(id);
        Client client = repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        User clientUser = client.getAccount();
        UserRepresentation userRepresentation = new UserRepresentation();
        if (body.getEmail() != null) {
            userRepresentation.setEmail(body.getEmail());
            clientUser.setEmail(body.getEmail());
        }
        if (body.getFirstName() != null) {
            userRepresentation.setFirstName(body.getFirstName());
            clientUser.setFirstName(body.getFirstName());
        }
        if (body.getLastName() != null) {
            userRepresentation.setLastName(body.getLastName());
            clientUser.setLastName(body.getLastName());
        }
        if (body.getNumeroTel() != null) {
            client.setNumeroTel(body.getNumeroTel());
            clientUser.setNumeroTel(body.getNumeroTel());
        }
        user.update(userRepresentation);
        client.setAccount(clientUser);


//        UserRepresentation response = usersRessource.get(id);
//        System.out.printf("Repsonse: %s %s %n", response.getStatus(), response.getStatusInfo());
        return repository.save(client);
    }

    public void deleteClient(String id) {
        RealmResource realmResource = keycloakBuilder().realm("ensapay");
        UsersResource usersRessource = realmResource.users();
        Response response = usersRessource.delete(id);
        System.out.printf("Repsonse: %s %s %n", response.getStatus(), response.getStatusInfo());
        repository.deleteById(id);
    }

    protected Keycloak keycloakBuilder() {
        return KeycloakBuilder.builder()
            .serverUrl("https://ensapay-auth-server.herokuapp.com/auth") //
            .realm("ensapay") //
            .grantType(OAuth2Constants.PASSWORD) //
            .clientId("resource-server") //
            .username("admin") //
            .password("pass") //
            .build();
    }

}
