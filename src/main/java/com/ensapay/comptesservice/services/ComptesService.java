package com.ensapay.comptesservice.services;

import com.ensapay.comptesservice.entities.Compte;
import com.ensapay.comptesservice.repositories.ComptesRepository;
import com.ensapay.comptesservice.utils.NumeroCompteGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
@Log4j2
public class ComptesService {
    private ComptesRepository comptesRepository;
    private ClientsService clientsService;

    public List<Compte> getComptes() {
        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        System.out.printf("Current user name %s", principal.getName());
        if (principal.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_agent")))
            return comptesRepository.findAll();
        return comptesRepository.findAllByClientId(principal.getName());
    }

    public Compte getCompte(String numeroCompte) {
        return getComptes().stream()
            .filter(compte -> compte.getNumeroCompte().equals(numeroCompte))
            .findFirst()
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Compte createCompte(Compte compte) {
        clientsService.getClient(compte.getClientId())
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Client introuvable"));
        compte.setNumeroCompte(NumeroCompteGenerator.generateMasterCardNumber());
        return comptesRepository.save(compte);
    }

    public Compte updateCompte(String numeroCompte, Compte body) {
        Compte compte = getCompte(numeroCompte);
        if (body.getSoldeFinal() != null)
            compte.setSoldeFinal(body.getSoldeFinal());
        if (body.getIntitule() != null)
            compte.setIntitule(body.getIntitule());
        return comptesRepository.save(compte);
    }

    public void deleteCompte(String numeroCompte) {
        comptesRepository.deleteById(numeroCompte);
    }



    protected KeycloakSecurityContext getKeycloakSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new IllegalStateException("Cannot set authorization header because there is no authenticated principal");
        } else if (!KeycloakAuthenticationToken.class.isAssignableFrom(authentication.getClass())) {
            throw new IllegalStateException(String.format("Cannot set authorization header because Authentication is of type %s but %s is required", authentication.getClass(), KeycloakAuthenticationToken.class));
        } else {
            KeycloakAuthenticationToken token = (KeycloakAuthenticationToken)authentication;
            KeycloakSecurityContext context = token.getAccount().getKeycloakSecurityContext();
            return context;
        }
    }

}
