package com.ensapay.comptesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ComptesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComptesServiceApplication.class, args);
	}

}
