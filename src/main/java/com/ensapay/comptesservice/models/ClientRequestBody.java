package com.ensapay.comptesservice.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ClientRequestBody {
    private String nom, prenom;
    private String email;
}
