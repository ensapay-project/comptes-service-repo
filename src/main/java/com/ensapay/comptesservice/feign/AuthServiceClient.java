package com.ensapay.comptesservice.feign;

import com.ensapay.comptesservice.config.FeignRequestInterceptor;
import com.ensapay.comptesservice.entities.Client;
import com.ensapay.comptesservice.entities.User;
import com.ensapay.comptesservice.models.ClientRequestBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "auth", url = "https://ensapay-auth-server.herokuapp.com/auth", configuration = FeignRequestInterceptor.class)
public interface AuthServiceClient {

    @PostMapping("/admin/realms/ensapay/users")
    Client createUser(@RequestBody User requestBody);

    @GetMapping("/admin/realms/ensapay/users")
    List<User> getUsers();
}
