package com.ensapay.comptesservice.repositories;

import com.ensapay.comptesservice.entities.Compte;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ComptesRepository extends MongoRepository<Compte,String> {
    List<Compte> findAllByClientId(String clientId);
}
