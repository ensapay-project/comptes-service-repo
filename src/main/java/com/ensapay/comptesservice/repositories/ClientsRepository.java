package com.ensapay.comptesservice.repositories;

import com.ensapay.comptesservice.entities.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ClientsRepository extends MongoRepository<Client, String> {
    Optional<Client> findByAccount_Email(String email);
}
